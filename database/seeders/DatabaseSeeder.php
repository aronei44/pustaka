<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\User;
use App\Models\Genre;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Admin",
            "email" => "admin@gmail.com",
            "password" => \Hash::make("admin123"),
            "is_active" => true,
            "role" => "admin",
        ]);
        $novel = Genre::create([
            "name" => "Novel",
        ]);
        $novel->books()->createMany([
            [
                "title" => "The Great Gatsby",
                "author" => "F. Scott Fitzgerald",
                "floor" => 1,
                "total_pcs" => 10,
                "genre_id" => $novel->id,
            ],
            [
                "title" => "Narnia. The Lion, the Witch and the Wardrobe",
                "author" => "C. S. Lewis",
                "floor" => 1,
                "total_pcs" => 10,
                "genre_id" => $novel->id,
            ],
            [
                "title" => "The Lord of the Rings",
                "author" => "J. R. R. Tolkien",
                "floor" => 1,
                "total_pcs" => 10,
                "genre_id" => $novel->id,
            ],
            [
                "title" => "The Hobbit",
                "author" => "J. R. R. Tolkien",
                "floor" => 1,
                "total_pcs" => 10,
                "genre_id" => $novel->id,
            ],
            [
                "title" => "The Catcher in the Rye",
                "author" => "J. D. Salinger",
                "floor" => 1,
                "total_pcs" => 10,
                "genre_id" => $novel->id,
            ],
            [
                "title" => "ulysses moore. the door of time",
                "author" => "Pierdomenico Baccalario",
                "floor" => 1,
                "total_pcs" => 10,
                "genre_id" => $novel->id,
            ]
        ]);
        $science = Genre::create([
            "name" => "Science",
        ]);
        $science->books()->createMany([
            [
                "title" => "The Elements of Physics",
                "author" => "Albert Einstein",
                "floor" => 2,
                "total_pcs" => 10,
                "genre_id" => $science->id,
            ],
            [
                "title" => "The Elements of Chemistry",
                "author" => "Hans Bethe",
                "floor" => 2,
                "total_pcs" => 10,
                "genre_id" => $science->id,
            ],
            [
                "title" => "The Elements of Mathematics",
                "author" => "Albert Einstein",
                "floor" => 2,
                "total_pcs" => 10,
                "genre_id" => $science->id,
            ],
            [
                "title" => "The Elements of Physics",
                "author" => "Albert Einstein",
                "floor" => 2,
                "total_pcs" => 10,
                "genre_id" => $science->id,
            ],
            [
                "title" => "The Elements of Chemistry",
                "author" => "Hans Bethe",
                "floor" => 2,
                "total_pcs" => 10,
                "genre_id" => $science->id,
            ],
            [
                "title" => "The Elements of Mathematics",
                "author" => "Albert Einstein",
                "floor" => 2,
                "total_pcs" => 10,
                "genre_id" => $science->id,
            ]
        ]);
        $history = Genre::create([
            "name" => "History",
        ]);
        $history->books()->createMany([
            [
                "title" => "Siddhartha",
                "author" => "Herman Hesse",
                "floor" => 3,
                "total_pcs" => 10,
                "genre_id" => $history->id,
            ],
            [
                "title" => "sun wukong",
                "author" => "Lao Tzu",
                "floor" => 3,
                "total_pcs" => 10,
                "genre_id" => $history->id,
            ],
            [
                "title" => "The Legend Of three Kingdoms",
                "author" => "Cao Xueqin",
                "floor" => 3,
                "total_pcs" => 10,
                "genre_id" => $history->id,
            ]
        ]);
    }
}
