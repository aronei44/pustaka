import React from "react";
import { Link } from "@inertiajs/inertia-react";
import { usePage } from "@inertiajs/inertia-react";
import { Inertia } from "@inertiajs/inertia";

const Nav = () => {
    const { user } = usePage().props;
    return (
        <div
            className="container">
            <nav
                className="navbar navbar-expand-lg bg-white shadow mt-4 rounded-pill"
                style={{
                    height:'8vh'
                }}>
                <div
                    className="container">
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span
                            className="navbar-toggler-icon" />
                    </button>
                    <div
                        className="collapse navbar-collapse"
                        id="navbarNavDropdown">
                        <ul
                            className="navbar-nav ms-auto px-5">
                            <li
                                className="nav-item dropdown">
                                <a
                                    className="nav-link dropdown-toggle text-dark"
                                    href="#"
                                    id="userDropdown"
                                    role="button"
                                    data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    {user.name}
                                </a>
                                <ul
                                    className="dropdown-menu"
                                    aria-labelledby="userDropdown">
                                    <li>
                                        <a
                                            onClick={()=>Inertia.post('/auth/logout')}
                                            className="dropdown-item bg-danger text-white"
                                            href="#">
                                            Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default Nav;
