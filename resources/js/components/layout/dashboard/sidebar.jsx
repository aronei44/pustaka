import React, { useState, useEffect} from "react";
import { Link } from "@inertiajs/inertia-react";
import { usePage } from "@inertiajs/inertia-react";
import Swal from "sweetalert2";

const SideBar = () => {
    const path = window.location.pathname;
    const { errors, success, server } = usePage().props;
    useEffect(() => {
        if(Object.keys(errors).length > 0){
            let text = '';
            for(let key in errors){
                text += `${key} : ${errors[key]} \n`;
            }
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: text,
            })
        }
        if(success !== null){
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: success,
            })
        }
        if(server !== null){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: server,
            })
        }
    }, [errors, success, server]);
    return (
        <div
            className="container">
            <div
                className="d-flex align-items-center"
                style={{
                    height: "10vh",
                }}>
                <Link
                    href="/"
                    style={{
                        color: "black",
                        textDecoration: "none"
                    }}>
                    <h1>Pustaka</h1>
                </Link>
            </div>
            <hr />
            <div
                className="list-group list-group-flush">
                <Link
                    href="/admin"
                    className={`list-group-item list-group-item-action ${path === '/admin' ? 'active':''}`}>
                    Dashboard
                </Link>
                <a
                    href="#"
                    className="list-group-item list-group-item-action disabled">
                    Master Data
                </a>
                <Link
                    href="/admin/genre"
                    className={`list-group-item list-group-item-action ${path === '/admin/genre' ? 'active':''}`}>
                    Genre
                </Link>
                <Link
                    href="/admin/book"
                    className={`list-group-item list-group-item-action ${path === '/admin/book' ? 'active':''}`}>
                    Buku
                </Link>
                <a
                    href="#"
                    className="list-group-item list-group-item-action disabled">
                    Otorisasi
                </a>
                <Link
                    href="/admin/peminjaman"
                    className={`list-group-item list-group-item-action ${path === '/admin/peminjaman' ? 'active':''}`}>
                    Peminjaman
                </Link>
                <Link
                    href="/admin/pengembalian"
                    className={`list-group-item list-group-item-action ${path === '/admin/pengembalian' ? 'active':''}`}>
                    Pengembalian
                </Link>
                <Link
                    href="/admin/history"
                    className={`list-group-item list-group-item-action ${path === '/admin/history' ? 'active':''}`}>
                    History
                </Link>
                <a
                    href="#"
                    className="list-group-item list-group-item-action disabled">
                    User
                </a>
                <Link
                    href="/admin/user-list"
                    className={`list-group-item list-group-item-action ${path === '/admin/user-list' ? 'active':''}`}>
                    List User
                </Link>
                <Link
                    href="/admin/otorisasi-user"
                    className={`list-group-item list-group-item-action ${path === '/admin/otorisasi-user' ? 'active':''}`}>
                    Otorisasi User
                </Link>
            </div>
        </div>
    )
}

export default SideBar;
