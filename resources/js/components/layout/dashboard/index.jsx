import React from "react";
import { Head, Link } from "@inertiajs/inertia-react";
import SideBar from "./sidebar";
import Nav from "./nav";

const DashboardLayout = ({children}) => {
    return (
        <>
        <Head title="Pustaka - Dashboard" />
            <div
                className="container-fluid">
                <div
                    className="row"
                    style={{
                        height: "100vh",
                    }}>
                    <div
                        className="col-md-3 bg-white shadow">
                        <SideBar />
                    </div>
                    <div
                        className="col-md-9">
                        <Nav />
                        <div
                            className="container mt-3"
                            style={{
                                overflowY: "auto",
                                height: "85vh",
                            }}>
                            {children}
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default DashboardLayout;
