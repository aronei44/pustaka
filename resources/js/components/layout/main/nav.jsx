import React, {useState, useEffect} from "react";
import { Link, usePage } from "@inertiajs/inertia-react";
import { Inertia } from "@inertiajs/inertia";
import axios from "axios";
import useSound from 'use-sound';
const Nav = () => {
    const { user } = usePage().props
    const [histories, setHistories] = useState([]);
    const [unRead, setUnRead] = useState(0);
    const [data, setData] = useState({});
    const [id, setId] = useState(0);
    const [i, setI] = useState(0);
    const [play] = useSound('/static/audio/song.mp3');
    useEffect(() => {
        if (user) {
            axios.get(`/history/data/${user.id}`)
            .then(res => {
                setHistories(res.data.histories)
            })
            .catch(err => {
                console.log(err)
            })
        }
    }, [user])
    useEffect(() => {
        let data = histories.filter(history => history.is_read === 0)
        setUnRead(data.length)
    }, [histories])
    useEffect(() => {
        if(id !== 0) {
            axios.get(`/history/detail/${id}`)
            .then(res => {
                setData(res.data.history)
                let newData = []
                histories.map(history => {
                    history.id === res.data.history.id ? newData.push(res.data.history) : newData.push(history)
                })
                setHistories(newData)
            })
            .catch(err => {
                console.log(err)
            })
        }
    }, [id])
    useEffect(() => {
        play()
    }, [i])
    Echo.channel('Pustaka')
		.listen('.borrowing', (e) => {
            if(e.user === user.id) {
                if(e.id !== i) {
                    setI(e.id)
                    setHistories([e.data,...histories])
                }
            }
		});
    return (
        <>
            <nav
                className="navbar navbar-expand-lg bg-white fixed-top shadow">
                <div
                    className="container">
                    <Link
                        className="navbar-brand text-dark"
                        href="/">
                        Pustaka
                    </Link>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span
                            className="navbar-toggler-icon" />
                    </button>
                    <div
                        className="collapse navbar-collapse"
                        id="navbarNavDropdown">
                        <ul
                            className="navbar-nav ms-auto">
                            {user?.role === "admin" && (
                                <li
                                    className="nav-item">
                                    <Link
                                        className="nav-link text-dark"
                                        href="/admin">
                                        Dashboard
                                    </Link>
                                </li>
                            ) }
                            <li
                                className="nav-item">
                                <Link
                                    className="nav-link text-dark"
                                    href="/book">
                                    Rak Buku
                                </Link>
                            </li>
                            {user && (
                                <>
                                    <li
                                        className="nav-item">
                                        <Link
                                            className="nav-link text-dark"
                                            href="/history">
                                            Sejarah Pinjamanku
                                        </Link>
                                    </li>
                                    <li
                                        className="nav-item">
                                        <a
                                            className="nav-link text-dark position-relative"
                                            data-bs-toggle="modal"
                                            data-bs-target="#historyModal"
                                            href="#">
                                            Notifikasi
                                            {unRead > 0 && (
                                                <span className="position-absolute top-10 start-200 translate-middle badge rounded-pill bg-danger">
                                                    {unRead}
                                                    <span className="visually-hidden">unread notification</span>
                                                </span>
                                            )}
                                        </a>
                                    </li>
                                    <li
                                        className="nav-item dropdown">
                                        <a
                                            className="nav-link dropdown-toggle text-dark"
                                            href="#"
                                            id="navbarDropdownMenuLink"
                                            role="button"
                                            data-bs-toggle="dropdown"
                                            aria-expanded="false">
                                            {user.name}
                                        </a>
                                        <ul
                                            className="dropdown-menu"
                                            aria-labelledby="navbarDropdownMenuLink">
                                            <li>
                                                <a
                                                    className="dropdown-item"
                                                    onClick={()=>Inertia.post('/auth/logout')}
                                                    href="#">
                                                    Logout
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </>
                            )}
                            {!user && (
                                <li
                                    className="nav-item">
                                    <Link
                                        className="nav-link btn btn-primary text-white"
                                        href="/auth">
                                        Login
                                    </Link>
                                </li>
                            )}
                        </ul>
                    </div>
                </div>
            </nav>
            <div
                className="modal fade"
                id="historyModal"
                tabIndex={-1}
                aria-labelledby="historyModalLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog modal-xl">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header">
                            <h5
                                className="modal-title"
                                id="historyModalLabel">
                                Notifikasi
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <div
                                className="container">
                                <div
                                    className="row"
                                    style={{
                                        maxHeight: "80vh",
                                    }}>
                                    <div
                                        className="col-md-3"
                                        style={{
                                            height: "75vh",
                                            maxHeight: "75vh",
                                            overflowY: "auto",
                                        }}>
                                        <div
                                            className="list-group">
                                            {histories.map((history, index) => {
                                                return (
                                                    <a
                                                        key={index}
                                                        onClick={() => setId(history.id)}
                                                        href="#"
                                                        className={`list-group-item list-group-item-action d-flex justify-content-between ${history.id === id? 'bg-primary text-white':''}`}>
                                                        <span
                                                            className={`text-${history.type} h4`}>
                                                            <i className={`bi
                                                                ${history.type === 'success' ? 'bi-check-circle' : ''}
                                                                ${history.type === 'danger' ? 'bi-exclamation-triangle' : ''}
                                                                ${history.type === 'info' ? 'bi-info' : ''}
                                                            `} />
                                                        </span>
                                                        <span>
                                                            {history.message.slice(0, 20)}...
                                                        </span>
                                                        <span
                                                            className="text-success">
                                                            <i className={`bi bi-${history.is_read ? 'check-all':'check'}`} />
                                                        </span>
                                                    </a>
                                                )
                                            })}
                                        </div>
                                    </div>
                                    <div
                                        className="col-md-9">
                                        {data && (
                                            <>
                                                <h5>{data.type}</h5>
                                                <hr />
                                                <p className="lead my-3">{data.message}</p>
                                                <hr />
                                                <p>{data.created_at}</p>
                                            </>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Nav;
