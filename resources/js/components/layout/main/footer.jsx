import React from "react";

const Footer = () => {
    return (
        <footer
            className="footer bg-dark text-white py-3">
            <div
                className="container">
                <div
                    className="row">
                    <div
                        className="col-md-6">
                        <h2
                            className="mb-3">
                            Pustaka Digi
                        </h2>
                        <div
                            className="d-flex mb-3">
                            <a
                                href="https://www.instagram.com/m.arwani.maulana">
                                <i className="bi-instagram h4 me-3 text-white" />
                            </a>
                            <a
                                href="https://www.facebook.com/arwani.maulana.5">
                                <i className="bi-facebook h4 me-3 text-white" />
                            </a>
                            <a
                                href="https://www.linkedin.com/in/muhamad-arwani-maulana-870297199">
                                <i className="bi-linkedin h4 me-3 text-white" />
                            </a>
                        </div>
                        <p>
                            &copy; Copyright Aronei44 | Megadigi 2022. All Rights Reserved.
                        </p>
                    </div>
                    <div
                        className="col-md-6">
                        <p>
                            Kp. Sirnagalih Rt 01/02
                        </p>
                        <p>
                            Desa Megamendung, Kecamatan Megamendung
                        </p>
                        <p>
                            Kabupaten Bogor. 16770
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer;
