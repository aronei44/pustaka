import React, {useEffect} from "react";
import Nav from "./nav";
import Footer from "./footer";
import { Head } from "@inertiajs/inertia-react";
import { usePage } from "@inertiajs/inertia-react";
import Swal from "sweetalert2";

const Layout = ({ children }) => {
    const { errors, success, server } = usePage().props;
    useEffect(() => {
        if(Object.keys(errors).length > 0){
            let text = '';
            for(let key in errors){
                text += `${key} : ${errors[key]} \n`;
            }
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: text,
            })
        }
        if(success !== null){
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: success,
            })
        }
        if(server !== null){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: server,
            })
        }
    }, [errors, success, server]);
    return (
        <>
            <Head title="Pustaka" />
            <Nav />
            <div
                className="container"
                style={{
                    minHeight: "100vh",
                    marginTop: "80px"
                }}>
                {children}
            </div>
            <Footer />
        </>
    );
}

export default Layout;
