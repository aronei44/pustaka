import React, {useState, useEffect} from "react";
import DashboardLayout from "../../layout/dashboard";

const Index = ({
    books,
    genres,
    users,
    borrows
}) => {
    const [totalBooks, setTotalBooks] = useState(0);
    const [borrowedBooks, setBorrowedBooks] = useState(0);
    useEffect(() => {
        let total = 0;
        let borrowed = 0;
        borrows.forEach(borrow => {
            if (borrow.status === "accepted") {
                borrowed += borrow.amount;
            }
            if (borrow.status !== "rejected") {
                total += borrow.amount;
            }
        })
        setTotalBooks(total);
        setBorrowedBooks(borrowed);
    }, [])
    return (
        <DashboardLayout>
            <div
                className="row mt-5">
                <div
                    className="col-md-4">
                    <div
                        className="card shadow-sm">
                        <div
                            className="card-body">
                            <h5
                                className="card-title">
                                Total Genre
                            </h5>
                            <h1
                                className="card-text my-4">
                                {genres}
                            </h1>
                        </div>
                    </div>
                </div>
                <div
                    className="col-md-4">
                    <div
                        className="card shadow-sm">
                        <div
                            className="card-body">
                            <h5
                                className="card-title">
                                Total Buku
                            </h5>
                            <h1
                                className="card-text my-4">
                                {books}
                            </h1>
                        </div>
                    </div>
                </div>
                <div
                    className="col-md-4">
                    <div
                        className="card shadow-sm">
                        <div
                            className="card-body">
                            <h5
                                className="card-title">
                                Total User
                            </h5>
                            <h1
                                className="card-text my-4">
                                {users}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div
                className="row mt-4">
                <div
                    className="col-md-6">
                    <div
                        className="card shadow-sm">
                        <div
                            className="card-body">
                            <h5
                                className="card-title">
                                Total Buku Dipinjam
                            </h5>
                            <h1
                                className="card-text my-4">
                                {totalBooks}
                            </h1>
                        </div>
                    </div>
                </div>
                <div
                    className="col-md-6">
                    <div
                        className="card shadow-sm">
                        <div
                            className="card-body">
                            <h5
                                className="card-title">
                                Total Buku Sedang Dipinjam
                            </h5>
                            <h1
                                className="card-text my-4">
                                {borrowedBooks}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </DashboardLayout>
    )
}

export default Index;
