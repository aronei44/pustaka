import { Inertia } from "@inertiajs/inertia";
import React, { useEffect, useState } from "react";
import DashboardLayout from "../../layout/dashboard";
import DataTable from "react-data-table-component";

const Peminjaman = ({
    borrows
}) => {
    const [reason, setReason] = useState("");
    const [id, setId] = useState("");
    const [filteredBorrows, setFilteredBorrows] = useState(borrows);
    const [search, setSearch] = useState({
        book: "",
        user: ""
    });
    useEffect(() => {
        const filtered = borrows.filter(borrow => {
            return borrow.book.title.toLowerCase().includes(search.book.toLowerCase()) && borrow.user.name.toLowerCase().includes(search.user.toLowerCase());
        });
        setFilteredBorrows(filtered);
    }, [search, borrows]);
    const reject = () => {
        Inertia.post(`/admin/user/borrow/${id}`,{
            status: "rejected",
            rejected_reason: reason
        })
    }
    return (
        <DashboardLayout>
            <ul
                className="nav justify-content-end bg-white mt-2 mb-2 shadow py-2 px-2">
                <li
                    className="nav-item">
                    <input
                        type="search"
                        className="form-control"
                        onChange={(e) => setSearch({
                            ...search,
                            book: e.target.value
                        })}
                        placeholder="cari judul buku"/>
                </li>
                <li
                    className="nav-item ms-2">
                    <input
                        type="search"
                        className="form-control"
                        onChange={(e) => setSearch({
                            ...search,
                            user: e.target.value
                        })}
                        placeholder="cari nama peminjam"/>
                </li>
            </ul>
            <div
                className="container bg-white shadow">
                <DataTable
                    title="Peminjaman"
                    pagination={true}
                    columns={[
                        {
                            name: 'No',
                            selector: (row, index) => index + 1,
                            sortable: false,
                        },
                        {
                            name: 'Nama Peminjam',
                            selector: 'user.name',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'Judul Buku',
                            selector: 'book.title',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'Jumlah',
                            selector: 'amount',
                            sortable: false,
                        },
                        {
                            name: 'Tanggal Pinjam',
                            selector: row => row.created_at.split(" ")[0],
                            sortable: true,
                        },
                        {
                            name: 'action',
                            selector: row => {
                                return (
                                    <>
                                        <button
                                            type="button"
                                            onClick={() => Inertia.post(`/admin/user/borrow/${row.id}`,{
                                                status: 'accepted'
                                            })}
                                            className="btn btn-warning me-3">
                                            <i class="bi bi-check"/>
                                        </button>
                                        <button
                                            type="button"
                                            onClick={() => setId(row.id)}
                                            className="btn btn-danger"
                                            data-bs-toggle="modal"
                                            data-bs-target="#reasonModal">
                                            <h6>X</h6>
                                        </button>
                                    </>
                                )
                            }
                        }
                    ]}
                    data={filteredBorrows}
                />
            </div>
            <div
                className="modal fade"
                id="reasonModal"
                tabIndex={-1}
                aria-labelledby="reasonModalLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header">
                            <h5
                                className="modal-title"
                                id="reasonModalLabel">
                                Tolak Permohonan Peminjaman
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <div
                                className="form-group">
                                <label
                                    htmlFor="reason">
                                    Alasan
                                </label>
                                <textarea
                                    className="form-control"
                                    id="reason"
                                    rows={3}
                                    onChange={(e) => setReason(e.target.value)}
                                    placeholder="Alasan ditolak"
                                    required />
                            </div>
                        </div>
                        <div
                            className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal">
                                Close
                            </button>
                            <button
                                type="button"
                                onClick={()=>reject()}
                                data-bs-dismiss="modal"
                                className="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </DashboardLayout>
    )
}

export default Peminjaman;
