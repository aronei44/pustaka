import { Inertia } from "@inertiajs/inertia";
import React, { useState, useEffect } from "react";
import DashboardLayout from "../../layout/dashboard";
import DataTable from "react-data-table-component";

const Pengembalian = ({
    borrows
}) => {
    const [filteredBorrows, setFilteredBorrows] = useState(borrows);
    const [search, setSearch] = useState({
        book: "",
        user: ""
    });
    useEffect(() => {
        const filtered = borrows.filter(borrow => {
            return borrow.book.title.toLowerCase().includes(search.book.toLowerCase()) && borrow.user.name.toLowerCase().includes(search.user.toLowerCase());
        });
        setFilteredBorrows(filtered);
    }, [search, borrows]);
    return (
        <DashboardLayout>
            <ul
                className="nav justify-content-end bg-white mt-2 mb-2 shadow py-2 px-2">
                <li
                    className="nav-item">
                    <input
                        type="search"
                        className="form-control"
                        onChange={(e) => setSearch({
                            ...search,
                            book: e.target.value
                        })}
                        placeholder="cari judul buku"/>
                </li>
                <li
                    className="nav-item ms-2">
                    <input
                        type="search"
                        className="form-control"
                        onChange={(e) => setSearch({
                            ...search,
                            user: e.target.value
                        })}
                        placeholder="cari nama peminjam"/>
                </li>
            </ul>
            <div
                className="container bg-white shadow">
                <DataTable
                    title="Pengembalian"
                    pagination={true}
                    columns={[
                        {
                            name: 'No',
                            selector: (row, index) => index + 1,
                            sortable: false,
                        },
                        {
                            name: 'Nama Peminjam',
                            selector: 'user.name',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'Judul Buku',
                            selector: 'book.title',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'Jumlah',
                            selector: 'amount',
                            sortable: false,
                        },
                        {
                            name: 'Tanggal Pinjam',
                            selector: row => row.created_at.split(" ")[0],
                            sortable: true,
                        },
                        {
                            name: 'action',
                            selector: row => {
                                return (
                                    <button
                                        type="button"
                                        onClick={() => Inertia.post(`/admin/user/return/${row.id}`)}
                                        className="btn btn-warning me-3">
                                        <i class="bi bi-check"/>
                                    </button>
                                )
                            }
                        }
                    ]}
                    data={filteredBorrows}
                />
            </div>

        </DashboardLayout>
    )
}

export default Pengembalian;
