import { Inertia } from "@inertiajs/inertia";
import React, { useEffect, useState } from "react";
import DashboardLayout from "../../layout/dashboard";
import DataTable from "react-data-table-component";

const Book = ({books, genres}) => {
    const [genre, setGenre] = useState('');
    const [errorGenre, setErrorGenre] = useState(true);
    const [data, setData] = useState({
        title: '',
        author: '',
        floor: '',
        total_pcs: '',
        genre_id: '',
    });
    const [errorData, setErrorData] = useState({
        title: true,
        author: true,
        floor: true,
        total_pcs: true,
        genre_id: true,
    });
    const [newData, setNewData] = useState({
        title: '',
        author: '',
        floor: '',
        total_pcs: '',
        genre_id: '',
        genre: {
            name: '',
        }
    });
    const [errorNewData, setErrorNewData] = useState({
        title: true,
        author: true,
        floor: true,
        total_pcs: true,
        genre_id: true,
    });
    const [filteredBooks, setFilteredBooks] = useState(books);
    const [filterData, setFilterData] = useState({
        title: '',
        author: '',
        genre: '',
    });

    useEffect(() => {
        if(genre === ''){
            setErrorGenre(true);
        } else {
            setErrorGenre(false);
        }
        setErrorData({
            title: data.title === '' ? true : false,
            author: data.author === '' ? true : false,
            floor: data.floor === '' ? true : false,
            total_pcs: data.total_pcs === '' ? true : false,
            genre_id: data.genre_id === '' ? true : false,
        });
        setErrorNewData({
            title: newData.title === '' ? true : false,
            author: newData.author === '' ? true : false,
            floor: newData.floor === '' ? true : false,
            total_pcs: newData.total_pcs === '' ? true : false,
            genre_id: newData.genre_id === '' ? true : false,
        });
        const filtered = books.filter(book => {
            return book.genre.name.toLowerCase().includes(filterData.genre.toLowerCase())
            && book.title.toLowerCase().includes(filterData.title.toLowerCase())
            && book.author.toLowerCase().includes(filterData.author.toLowerCase());
        });
        setFilteredBooks(filtered);
    }, [genre, data, newData, filterData]);

    const sendGenre = () => {
        Inertia.post("/admin/genre", {
            name: genre
        });
    }
    const send = () => {
        Inertia.post("/admin/book", data);
        setData({
            title: '',
            author: '',
            floor: '',
            total_pcs: '',
            genre_id: '',
        });
    }
    const update = () => {
        Inertia.put(`/admin/book/${newData.id}`, newData);
        setNewData({
            title: '',
            author: '',
            floor: '',
            total_pcs: '',
            genre_id: '',
            genre: {
                name: '',
            }
        });
    }
    const destroy = () => {
        Inertia.delete(`/admin/book/${newData.id}`);
    }
    return (
        <DashboardLayout>
            <ul
                className="nav justify-content-end bg-white mt-2 mb-2 shadow py-2 px-2">
                <li
                    className="nav-item me-2">
                    <input
                        className="form-control"
                        type="search"
                        placeholder="Cari Judul"
                        value={filterData.title}
                        onChange={(e) => setFilterData({
                            ...filterData,
                            title: e.target.value
                        })}
                    />
                </li>
                <li
                    className="nav-item me-2">
                    <input
                        className="form-control"
                        type="search"
                        placeholder="Cari Penulis"
                        value={filterData.author}
                        onChange={(e) => setFilterData({
                            ...filterData,
                            author: e.target.value
                        })}
                    />
                </li>
                <li
                    className="nav-item me-2">
                    <input
                        className="form-control"
                        type="search"
                        placeholder="Cari Genre"
                        value={filterData.genre}
                        onChange={(e) => setFilterData({
                            ...filterData,
                            genre: e.target.value
                        })}
                    />
                </li>
                <li
                    className="nav-item">
                    <button
                        type="button"
                        className="btn btn-success"
                        data-bs-toggle="modal"
                        data-bs-target="#addGenre">
                        Tambah Genre
                    </button>
                </li>
                <li
                    className="nav-item ms-2">
                    <button
                        type="button"
                        className="btn btn-primary"
                        data-bs-toggle="modal"
                        data-bs-target="#addBook">
                        Tambah Buku
                    </button>
                </li>
            </ul>
            <div
                className="container bg-white shadow">
                <DataTable
                    title="Daftar Buku"
                    columns={[
                        {
                            name: 'No',
                            selector: (row, index) => index + 1,
                            sortable: false,
                        },
                        {
                            name: 'Judul',
                            selector: 'title',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'Penulis',
                            selector: 'author',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'Lantai',
                            selector: 'floor',
                            sortable: true,
                        },
                        {
                            name: 'Total',
                            selector: 'total_pcs',
                            sortable: true,
                        },
                        {
                            name: 'Genre',
                            selector: 'genre.name',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'action',
                            selector: (row) => {
                                return (
                                    <div>
                                        <button
                                            type="button"
                                            className="btn btn-primary"
                                            data-bs-toggle="modal"
                                            data-bs-target="#editBook"
                                            onClick={() => {
                                                setNewData(row)
                                            }}>
                                            <i className="bi bi-pencil-square"/>
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-danger"
                                            data-bs-toggle="modal"
                                            data-bs-target="#deleteBook"
                                            onClick={() => {
                                                setNewData(row)
                                            }}>
                                            <i className="bi bi-trash"/>
                                        </button>
                                    </div>
                                )
                            },
                            sortable: false,
                        },
                    ]}
                    data={filteredBooks}
                    pagination={true}
                />
            </div>
            {/* modal add genre */}
            <div
                className="modal fade"
                id="addGenre"
                tabIndex={-1}
                aria-labelledby="addGenreLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header">
                            <h5
                                className="modal-title"
                                id="addGenreLabel">
                                Tambah Genre
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <div
                                className="form-group">
                                <label
                                    htmlFor="genre">
                                    Nama Genre
                                </label>
                                <input
                                    type="text"
                                    onChange={e => setGenre(e.target.value)}
                                    className={`form-control ${errorGenre ? 'is-invalid' : 'is-valid'}`}
                                    placeholder="Genre" />
                                <div className={`${errorGenre ? 'invalid-feedback':'valid-feedback' }`}>
                                    {errorGenre ? 'Genre tidak boleh kosong' : ''}
                                </div>
                            </div>
                        </div>
                        <div
                            className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal">
                                Close
                            </button>
                            <button
                                type="button"
                                disabled={errorGenre}
                                onClick={()=>sendGenre()}
                                data-bs-dismiss="modal"
                                className="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {/* modal add book */}
            <div
                className="modal fade"
                id="addBook"
                tabIndex={-1}
                aria-labelledby="addBookLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header">
                            <h5
                                className="modal-title"
                                id="addBookLabel">
                                Tambah Buku
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <div
                                className="form-group">
                                <label
                                    htmlFor="title">
                                    Judul Buku
                                </label>
                                <input
                                    type="text"
                                    value={data.title}
                                    onChange={e=> setData({...data, title: e.target.value})}
                                    className={`form-control ${errorData.title ? 'is-invalid' : 'is-valid'}`}
                                    id="title"
                                    placeholder="Judul Buku"
                                    />
                            </div>
                            <div
                                className="form-group">
                                <label
                                    htmlFor="author">
                                    Penulis
                                </label>
                                <input
                                    type="text"
                                    value={data.author}
                                    onChange={e=> setData({...data, author: e.target.value})}
                                    className={`form-control ${errorData.author ? 'is-invalid' : 'is-valid'}`}
                                    id="author"
                                    placeholder="Penulis"
                                    />
                            </div>
                            <div
                                className="form-group">
                                <label
                                    htmlFor="floor">
                                    Lantai
                                </label>
                                <input
                                    type="text"
                                    value={data.floor}
                                    onChange={e=> setData({...data, floor: e.target.value})}
                                    className={`form-control ${errorData.floor ? 'is-invalid' : 'is-valid'}`}
                                    onKeyDown={(e) => e.target.value = e.target.value.replace(/[^\d]/,'')}
                                    onKeyUp={(e) => e.target.value = e.target.value.replace(/[^\d]/,'')}
                                    id="floor"
                                    placeholder="Lantai"
                                    />
                            </div>
                            <div
                                className="form-group">
                                <label
                                    htmlFor="total_pcs">
                                    Total Buku
                                </label>
                                <input
                                    type="text"
                                    value={data.total_pcs}
                                    onChange={e=> setData({...data, total_pcs: e.target.value})}
                                    className={`form-control ${errorData.total_pcs ? 'is-invalid' : 'is-valid'}`}
                                    onKeyDown={(e) => e.target.value = e.target.value.replace(/[^\d]/,'')}
                                    onKeyUp={(e) => e.target.value = e.target.value.replace(/[^\d]/,'')}
                                    id="total_pcs"
                                    placeholder="Total Buku"
                                    />
                            </div>
                            <div
                                className="form-group">
                                <label
                                    htmlFor="genre">
                                    Genre
                                </label>
                                <select
                                    className={`form-control ${errorData.genre_id ? 'is-invalid' : 'is-valid'}`}
                                    onChange={e=> setData({...data, genre_id: e.target.value})}
                                    id="genre">
                                    <option selected disabled>
                                        {genres.length === 0 ? 'Anda Belum Punya Genre' : 'Pilih Genre'}
                                    </option>
                                    {
                                        genres.map((genre, index) => {
                                            return (
                                                <option
                                                    key={index}
                                                    value={genre.id}>
                                                    {genre.name}
                                                </option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                        </div>
                        <div
                            className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal">
                                Close
                            </button>
                            <button
                                type="button"
                                disabled={errorData.title || errorData.author || errorData.floor || errorData.total_pcs || errorData.genre_id}
                                onClick={()=>send()}
                                data-bs-dismiss="modal"
                                className="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {/* modal edit book */}
            <div
                className="modal fade"
                id="editBook"
                tabIndex={-1}
                aria-labelledby="editBookLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header">
                            <h5
                                className="modal-title"
                                id="editBookLabel">
                                Edit Buku
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <div
                                className="form-group">
                                <label
                                    htmlFor="title">
                                    Judul Buku
                                </label>
                                <input
                                    type="text"
                                    value={newData.title}
                                    onChange={e=> setNewData({...newData, title: e.target.value})}
                                    className={`form-control ${errorNewData.title ? 'is-invalid' : 'is-valid'}`}
                                    id="title"
                                    placeholder="Judul Buku"
                                    />
                            </div>
                            <div
                                className="form-group">
                                <label
                                    htmlFor="author">
                                    Penulis
                                </label>
                                <input
                                    type="text"
                                    value={newData.author}
                                    onChange={e=> setNewData({...newData, author: e.target.value})}
                                    className={`form-control ${errorNewData.author ? 'is-invalid' : 'is-valid'}`}
                                    id="author"
                                    placeholder="Penulis"
                                    />
                            </div>
                            <div
                                className="form-group">
                                <label
                                    htmlFor="floor">
                                    Lantai
                                </label>
                                <input
                                    type="text"
                                    value={newData.floor}
                                    onChange={e=> setNewData({...newData, floor: e.target.value})}
                                    className={`form-control ${errorNewData.floor ? 'is-invalid' : 'is-valid'}`}
                                    onKeyDown={(e) => e.target.value = e.target.value.replace(/[^\d]/,'')}
                                    onKeyUp={(e) => e.target.value = e.target.value.replace(/[^\d]/,'')}
                                    id="floor"
                                    placeholder="Lantai"
                                    />
                            </div>
                            <div
                                className="form-group">
                                <label
                                    htmlFor="total_pcs">
                                    Total Buku
                                </label>
                                <input
                                    type="text"
                                    value={newData.total_pcs}
                                    onChange={e=> setNewData({...newData, total_pcs: e.target.value})}
                                    className={`form-control ${errorNewData.total_pcs ? 'is-invalid' : 'is-valid'}`}
                                    onKeyDown={(e) => e.target.value = e.target.value.replace(/[^\d]/,'')}
                                    onKeyUp={(e) => e.target.value = e.target.value.replace(/[^\d]/,'')}
                                    id="total_pcs"
                                    placeholder="Total Buku"
                                    />
                            </div>
                            <div
                                className="form-group">
                                <label
                                    htmlFor="genre">
                                    Genre
                                </label>
                                <select
                                    className={`form-control ${errorNewData.genre_id ? 'is-invalid' : 'is-valid'}`}
                                    onChange={e=> setNewData({...newData, genre_id: e.target.value})}
                                    id="genre">
                                    {
                                        genres.map((genre, index) => {
                                            return (
                                                <option
                                                    key={index}
                                                    selected={newData.genre_id === genre.id}
                                                    value={genre.id}>
                                                    {genre.name}
                                                </option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                        </div>
                        <div
                            className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal">
                                Close
                            </button>
                            <button
                                type="button"
                                disabled={errorNewData.title || errorNewData.author || errorNewData.floor || errorNewData.total_pcs || errorNewData.genre_id}
                                onClick={()=>update()}
                                data-bs-dismiss="modal"
                                className="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {/* modal delete genre */}
            <div
                className="modal fade"
                id="deleteBook"
                tabIndex={-1}
                aria-labelledby="deleteBookLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header bg-danger text-white">
                            <h5
                                className="modal-title"
                                id="deleteBookLabel">
                                Delete Book
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <h4>Are You Sure Delete This Data?</h4>
                            <table
                                className="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <b>Title</b>
                                        </td>
                                        <td>
                                            : {newData.title}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Author</b>
                                        </td>
                                        <td>
                                            : {newData.author}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Floor</b>
                                        </td>
                                        <td>
                                            : {newData.floor}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Total Books</b>
                                        </td>
                                        <td>
                                            : {newData.total_pcs}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Genre</b>
                                        </td>
                                        <td>
                                            : {newData.genre.name}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div
                            className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal">
                                Close
                            </button>
                            <button
                                type="button"
                                onClick={()=>destroy()}
                                data-bs-dismiss="modal"
                                className="btn btn-danger">
                                Delete
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </DashboardLayout>
    )
}

export default Book;
