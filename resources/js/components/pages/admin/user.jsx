import React, { useState, useEffect } from "react";
import DashboardLayout from "../../layout/dashboard";
import DataTable from "react-data-table-component";

const User = ({
    users
}) => {
    const [filteredUsers, setFilteredUsers] = useState(users);
    const [search, setSearch] = useState({
        name: "",
        email: "",
        role: ""
    });
    useEffect(() => {
        const filtered = users.filter(user => {
            return user.name.toLowerCase().includes(search.name.toLowerCase())
            && user.email.toLowerCase().includes(search.email.toLowerCase())
            && user.role.toLowerCase().includes(search.role.toLowerCase());
        });
        setFilteredUsers(filtered);
    }, [search, users]);
    return (
        <DashboardLayout>
            <ul
                className="nav justify-content-end bg-white mt-2 mb-2 shadow py-2 px-2">
                <li
                    className="nav-item">
                    <input
                        type="search"
                        className="form-control"
                        onChange={(e) => setSearch({
                            ...search,
                            name: e.target.value
                        })}
                        placeholder="cari nama"/>
                </li>
                <li
                    className="nav-item ms-2">
                    <input
                        type="search"
                        className="form-control"
                        onChange={(e) => setSearch({
                            ...search,
                            email: e.target.value
                        })}
                        placeholder="cari email"/>
                </li>
                <li
                    className="nav-item ms-2">
                    <input
                        type="search"
                        className="form-control"
                        onChange={(e) => setSearch({
                            ...search,
                            role: e.target.value
                        })}
                        placeholder="cari role"/>
                </li>
            </ul>
            <div
                className="container bg-white shadow">
                <DataTable
                    title="User"
                    pagination={true}
                    columns={[
                        {
                            name: 'No',
                            selector: (row, index) => index + 1,
                            sortable: false,
                        },
                        {
                            name: 'Nama',
                            selector: 'name',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'Email',
                            selector: 'email',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'Role',
                            selector: 'role',
                            sortable: true,
                        },
                        {
                            name: 'Tanggal Bergabung',
                            selector: row => row.created_at.split(" ")[0],
                            sortable: true,
                        }
                    ]}
                    data={filteredUsers}
                />
            </div>

        </DashboardLayout>
    )
}

export default User;
