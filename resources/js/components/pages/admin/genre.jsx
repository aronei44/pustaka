import { Inertia } from "@inertiajs/inertia";
import React, { useEffect, useState } from "react";
import DashboardLayout from "../../layout/dashboard";
import DataTable from "react-data-table-component";

const Genre = ({genres}) => {
    const [genre, setGenre] = useState('');
    const [errorGenre, setErrorGenre] = useState(true);
    const [data, setData] = useState({});
    const [newGenre, setNewGenre] = useState('');
    const [errorNewGenre, setErrorNewGenre] = useState(false);
    const [filteredGenres, setFilteredGenres] = useState(genres);
    const [search, setSearch] = useState('');
    useEffect(() => {
        if(genre === ''){
            setErrorGenre(true);
        } else {
            setErrorGenre(false);
        }
        if(newGenre === ''){
            setErrorNewGenre(true);
        } else {
            setErrorNewGenre(false);
        }
    }, [genre, newGenre]);
    useEffect(() => {
        if(search === ''){
            setFilteredGenres(genres);
        } else {
            const filtered = genres.filter(genre => {
                return genre.name.toLowerCase().includes(search.toLowerCase());
            });
            setFilteredGenres(filtered);
        }
    }, [search, genres]);
    const send = () => {
        Inertia.post("/admin/genre", {
            name: genre
        });
        setGenre('');
    }
    const update = () => {
        Inertia.put(`/admin/genre/${data.id}`,{
            name: newGenre
        })
        setNewGenre('');
    }
    const destroy = () => {
        Inertia.delete(`/admin/genre/${data.id}`)
    }
    return (
        <DashboardLayout>
            <ul
                className="nav justify-content-end bg-white mt-2 mb-2 shadow py-2 px-2">
                <li
                    className="nav-item">
                    <input
                        type="search"
                        className="form-control"
                        placeholder="cari nama genre"
                        value={search}
                        onChange={(e) => setSearch(e.target.value)}/>
                </li>
                <li
                    className="nav-item ms-2">
                    <button
                        type="button"
                        className="btn btn-primary"
                        data-bs-toggle="modal"
                        data-bs-target="#addGenre">
                        Tambah Genre
                    </button>
                </li>
            </ul>
            <div
                className="container bg-white shadow">
                <DataTable
                    title="Genre"
                    pagination={true}
                    columns={[
                        {
                            name: 'No',
                            selector: (row, index) => index + 1,
                            sortable: false,
                        },
                        {
                            name: 'Nama',
                            selector: 'name',
                            sortable: true,
                            wrap: true,
                        },
                        {
                            name: 'Total Buku',
                            selector: row => row.books.length,
                            sortable: false,
                        },
                        {
                            name: 'Action',
                            cell: row => {
                                return (
                                    <>
                                        <button
                                            type="button"
                                            onClick={() => {
                                                setData(row)
                                                setNewGenre(row.name)
                                            }}
                                            className="btn btn-warning me-3"
                                            data-bs-toggle="modal"
                                            data-bs-target="#editGenre">
                                            <i class="bi bi-pencil-square"/>
                                        </button>
                                        <button
                                            type="button"
                                            onClick={() => setData(row)}
                                            className="btn btn-danger"
                                            data-bs-toggle="modal"
                                            data-bs-target="#deleteGenre">
                                            <i class="bi bi-trash"/>
                                        </button>
                                    </>
                                )
                            }
                        }
                    ]}
                    data={filteredGenres}
                />
            </div>

            {/* modal add genre */}
            <div
                className="modal fade"
                id="addGenre"
                tabIndex={-1}
                aria-labelledby="addGenreLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header">
                            <h5
                                className="modal-title"
                                id="addGenreLabel">
                                Tambah Genre
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <div
                                className="form-group">
                                <label
                                    htmlFor="genre">
                                    Nama Genre
                                </label>
                                <input
                                    type="text"
                                    onChange={e => setGenre(e.target.value)}
                                    className={`form-control ${errorGenre ? 'is-invalid' : 'is-valid'}`}
                                    placeholder="Genre" />
                                <div className={`${errorGenre ? 'invalid-feedback':'valid-feedback' }`}>
                                    {errorGenre ? 'Genre tidak boleh kosong' : ''}
                                </div>
                            </div>
                        </div>
                        <div
                            className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal">
                                Close
                            </button>
                            <button
                                type="button"
                                disabled={errorGenre}
                                onClick={()=>send()}
                                data-bs-dismiss="modal"
                                className="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {/* modal edit genre */}
            <div
                className="modal fade"
                id="editGenre"
                tabIndex={-1}
                aria-labelledby="editGenreLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header">
                            <h5
                                className="modal-title"
                                id="editGenreLabel">
                                Edit Genre
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <div
                                className="form-group">
                                <label
                                    htmlFor="genre">
                                    Nama Genre
                                </label>
                                <input
                                    type="text"
                                    value={newGenre}
                                    onChange={e => setNewGenre(e.target.value)}
                                    className={`form-control ${errorNewGenre ? 'is-invalid' : 'is-valid'}`}
                                    placeholder="Genre" />
                                <div className={`${errorNewGenre ? 'invalid-feedback':'valid-feedback' }`}>
                                    {errorNewGenre ? 'Genre tidak boleh kosong' : ''}
                                </div>
                            </div>
                        </div>
                        <div
                            className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal">
                                Close
                            </button>
                            <button
                                type="button"
                                disabled={errorNewGenre}
                                onClick={()=>update()}
                                data-bs-dismiss="modal"
                                className="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {/* modal delete genre */}
            <div
                className="modal fade"
                id="deleteGenre"
                tabIndex={-1}
                aria-labelledby="deleteGenreLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header bg-danger text-white">
                            <h5
                                className="modal-title"
                                id="deleteGenreLabel">
                                Delete Genre
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <h4>Are You Sure Delete This Data?</h4>
                            <h5>Total Books Will Be Deleted : {data.books?.length}</h5>
                            <p>Genre : {data.name}</p>
                        </div>
                        <div
                            className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal">
                                Close
                            </button>
                            <button
                                type="button"
                                onClick={()=>destroy()}
                                data-bs-dismiss="modal"
                                className="btn btn-danger">
                                Delete
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </DashboardLayout>
    )
}

export default Genre;
