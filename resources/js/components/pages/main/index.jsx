import { Link } from '@inertiajs/inertia-react';
import React from 'react'
import Layout from '../../layout/main/layout'

const Index = () => {
  return (
    <Layout>
        <div
            className="container">
            <div
                className="row align-items-center">
                <div
                    className="col-md-6">
                    <img
                        src="/img/main.png"
                        alt="main"
                        className="img-fluid" />
                </div>
                <div
                    className="col-md-6">
                    <h2>Selamat Datang di Pustaka Digital</h2>
                    <p>Atur Pinjaman dan Pengembalian buku dengan mudah.</p>
                    <Link
                        href="/book"
                        className="btn btn-primary">
                        Mulai
                    </Link>
                </div>
            </div>
        </div>
    </Layout>
  )
}
export default Index;
