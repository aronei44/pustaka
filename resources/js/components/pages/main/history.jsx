import React, {useState, useEffect} from 'react'
import Layout from '../../layout/main/layout'
import { Inertia } from '@inertiajs/inertia'
import DataTable from 'react-data-table-component'

const History = ({
    borrows
}) => {
    const [filteredBorrows, setFilteredBorrows] = useState(borrows)
    const [search, setSearch] = useState({
        title: '',
        status: '',
    })
    useEffect(() => {
        setFilteredBorrows(borrows)
    }, [borrows])
    useEffect(() => {
        let data = borrows.filter(borrow => {
            return borrow.book.title.toLowerCase().includes(search.title.toLowerCase()) && borrow.status.toLowerCase().includes(search.status.toLowerCase())
        })
        setFilteredBorrows(data)
    }, [search])
  return (
    <Layout>
        <ul
            className="nav justify-content-end bg-white mt-2 mb-2 shadow py-2 px-2">
            <li
                className="nav-item">
                <input
                    type="search"
                    className="form-control"
                    onChange={(e) => setSearch({...search, title: e.target.value})}
                    placeholder="cari judul buku" />
            </li>
            <li
                className="nav-item ms-2">
                <input
                    type="search"
                    className="form-control"
                    onChange={(e) => setSearch({...search, status: e.target.value})}
                    placeholder="cari status peminjaman" />
            </li>
        </ul>
        <div
            className="container bg-white shadow">
            <DataTable
                title="History"
                pagination={true}
                columns={[
                    {
                        name: 'No',
                        selector: (row, index) => index + 1,
                        sortable: false,
                    },
                    {
                        name: 'Judul Buku',
                        selector: 'book.title',
                        sortable: true,
                        wrap: true,
                    },
                    {
                        name: 'Jumlah',
                        selector: 'amount',
                        sortable: false,
                    },
                    {
                        name: 'Tanggal Pinjam',
                        selector: row => row.created_at.split(" ")[0],
                        sortable: true,
                    },
                    {
                        name: 'Tanggal Kembali',
                        selector: row => row.returned_at?.split(" ")[0],
                        sortable: true,
                    },
                    {
                        name: 'Status',
                        selector: row => row.status,
                        sortable: true,
                    },
                    {
                        name: 'action',
                        selector: row => {
                            if(row.status === 'accepted'){
                                return (
                                    <button
                                        type="button"
                                        onClick={() => Inertia.post(`/book/return/${row.id}`)}
                                        className="btn btn-primary">
                                        Kembalikan
                                    </button>
                                )
                            }
                            return (<></>)
                        }

                    }
                ]}
                data={filteredBorrows}
            />
        </div>
    </Layout>
  )
}
export default History;
