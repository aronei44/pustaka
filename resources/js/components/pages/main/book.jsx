import React, { useState, useEffect } from "react";
import Layout from "../../layout/main/layout";
import { usePage } from "@inertiajs/inertia-react";
import { Inertia } from "@inertiajs/inertia";

const Book = ({books}) => {
    const { user } = usePage().props
    const [cart, setCart] = useState([]);
    const [filteredBooks, setFilteredBooks] = useState(books);
    const [search, setSearch] = useState({
        title: "",
        author: "",
        genre: "",
    });
    useEffect(() => {
        const filtered = books.filter(book => {
            return book.title.toLowerCase().includes(search.title.toLowerCase()) &&
                book.author.toLowerCase().includes(search.author.toLowerCase()) &&
                book.genre.name.toLowerCase().includes(search.genre.toLowerCase());
        });
        setFilteredBooks(filtered);
    }, [search]);
    const [height, setHeight] = useState(0);
    useEffect(() => {
        setTimeout(() => {
            const h = document.getElementById('content').innerHeight;
            setHeight(h);
        }, 1000);
    }, []);
    let width = window.innerWidth;
    return (
        <Layout>
            <div
                className="container">
                <div
                    className="row">
                    <div
                        className="col-md-9">
                        {books.length < 1 ? (
                            <p>Data Buku Tidak Ditemukan</p>
                        ):(
                            <div
                                className="container bg-white d-flex flex-wrap shadow py-3">
                                {filteredBooks.map((book, index) => {
                                    return (
                                        <div
                                            key={index}
                                            id="content"
                                            className="card mb-1 me-1"
                                            style={{
                                                width: width > 900 ? "15rem" : width > 500 ? "45%" : "100%",
                                                minWidth: width > 900 ? "15rem" : width > 500 ? "45%" : "100%"
                                            }}>
                                            <div
                                                className="pt-2 mx-auto"
                                                style={{
                                                    height: "200px",
                                                    width: "90%",
                                                    overflow: "hidden"
                                                }}>
                                                <img
                                                    src="/img/buku.jpg"
                                                    className="card-img-top"
                                                    alt="buku.jpg" />
                                            </div>
                                            <div
                                                className="card-body">
                                                <h5
                                                    className="card-title">
                                                    {book.title} - {book.author}
                                                </h5>
                                                <span className="bg-info px-2 rounded-pill">
                                                    {book.genre.name}
                                                </span>
                                                <p
                                                    className="card-text">
                                                    Tersedia : {book.total_pcs - book.borrowed_pcs} <br />
                                                    Lantai : {book.floor}
                                                </p>
                                                {user ? (
                                                    <div
                                                        className="d-flex justify-content-between">
                                                        <button
                                                            onClick={()=>Inertia.post('/book/borrow', {id: book.id, amount:1})}
                                                            className="btn btn-primary">
                                                            Pinjam
                                                        </button>
                                                        {cart.find(item => item.id === book.id) ? (
                                                            <button
                                                                onClick={()=>setCart(cart.filter(item => item.id !== book.id))}
                                                                className="btn btn-danger">
                                                                Hapus
                                                            </button>
                                                        ):(
                                                            <button
                                                                disabled={book.total_pcs - book.borrowed_pcs < 1}
                                                                onClick={()=>setCart([...cart, {...book, amount:1}])}
                                                                className="btn btn-success">
                                                                {book.total_pcs - book.borrowed_pcs < 1 ? "Habis" : "Keranjang"}
                                                            </button>
                                                        )}
                                                    </div>
                                                ):(
                                                    <p>
                                                        Silahkan login untuk melakukan peminjaman buku
                                                    </p>
                                                )}
                                            </div>
                                        </div>

                                    )
                                })}
                            </div>

                        )}
                    </div>
                    <div
                        className="col-md-3"
                        style={{
                            display: "flex",
                            alignItems: "flex-start",
                            height: height,
                            marginTop: width > 750 ? "0px" : "50px"
                        }}>
                        <div
                            style={{
                                position: "sticky",
                                top: "5rem",
                            }}>

                            <div
                                className="bg-white shadow-sm container py-3">
                                <h5>Filter Buku</h5>
                                <input
                                    type='search'
                                    value={search.title}
                                    onChange={(e) => setSearch({
                                        ...search,
                                        title: e.target.value
                                    })}
                                    className='form-control'
                                    placeholder='Judul Buku' />
                                <input
                                    type='search'
                                    value={search.author}
                                    onChange={(e) => setSearch({
                                        ...search,
                                        author: e.target.value
                                    })}
                                    className='form-control mt-3'
                                    placeholder='Penulis' />
                                <input
                                    type='search'
                                    value={search.genre}
                                    onChange={(e) => setSearch({
                                        ...search,
                                        genre: e.target.value
                                    })}
                                    className='form-control mt-3'
                                    placeholder='Genre' />
                            </div>
                            <div
                                className="bg-white shadow-sm container py-3 mt-3">
                                <h5>
                                    Keranjang
                                </h5>
                                <p>
                                    Jumlah Buku : {cart.length}
                                </p>
                                <div
                                    className="d-flex justify-content-between">
                                    <button
                                        onClick={()=>setCart([])}
                                        className="btn btn-danger">
                                        Hapus Semua
                                    </button>
                                    <button
                                        data-bs-toggle="modal"
                                        data-bs-target="#detailBook"
                                        className="btn btn-primary">
                                        Detail
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* modal add genre */}
            <div
                className="modal fade"
                id="detailBook"
                tabIndex={-1}
                aria-labelledby="detailBookLabel"
                aria-hidden="true">
                <div
                    className="modal-dialog  modal-lg">
                    <div
                        className="modal-content">
                        <div
                            className="modal-header">
                            <h5
                                className="modal-title"
                                id="detailBookLabel">
                                Detail
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close" />
                        </div>
                        <div
                            className="modal-body">
                            <table
                                className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Judul</th>
                                        <th>Jumlah</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {cart.map((item, index) => (
                                        <tr key={index}>
                                            <td>{item.title}</td>
                                            <td>
                                                <button
                                                    onClick={()=>{
                                                        let data = cart;
                                                        data[index].amount = data[index].amount - 1;
                                                        if(data[index].amount < 1){
                                                            data = data.filter(i => i.id !== item.id);
                                                        }
                                                        setCart([...data]);
                                                    }}
                                                    className="btn btn-sm btn-info me-1">
                                                    -
                                                </button>
                                                {item.amount}
                                                <button
                                                    onClick={()=>{
                                                        let data = cart;
                                                        if(item.total_pcs - item.borrowed_pcs > data[index].amount){
                                                            data[index].amount = data[index].amount + 1;
                                                        }
                                                        setCart([...data]);
                                                    }}
                                                    className="btn btn-sm btn-info ms-1">
                                                    +
                                                </button>
                                            </td>
                                            <td>
                                                <button
                                                    onClick={()=>setCart(cart.filter(i => i.id !== item.id))}
                                                    className="btn btn-sm btn-danger">
                                                    <i className="bi bi-trash" />
                                                </button>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                        <div
                            className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal">
                                Close
                            </button>
                            <button
                                type="button"
                                onClick={()=>Inertia.post(`/book/borrows`, {items:cart})}
                                data-bs-dismiss="modal"
                                className="btn btn-primary">
                                Pinjam Sekarang
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default Book;
