import React from 'react'
import { render } from 'react-dom'
import { createInertiaApp } from '@inertiajs/inertia-react'
import { InertiaProgress } from '@inertiajs/progress'
require('./bootstrap')

InertiaProgress.init()
createInertiaApp({
  resolve: name => import(`./components/pages/${name}`),
  setup({ el, App, props }) {
    render(<App {...props} />, el)
  },
})

