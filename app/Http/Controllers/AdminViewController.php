<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminViewController extends Controller
{
    // view dashboard index
    public function index()
    {
        return Inertia::render('admin',[
            'books' => count(DB::table('books')->get()),
            'genres' => count(DB::table('genres')->get()),
            'users' => count(DB::table('users')->get()),
            'borrows' => DB::table('borrows')->get(),
        ]);
    }
    // view dashboard genre
    public function genreView()
    {
        $genres = DB::table('genres')->get();
        foreach ($genres as $genre) {
            $genre->books = DB::table('books')->where('genre_id', $genre->id)->get();
        }
        return Inertia::render('admin/genre',[
            'genres' => $genres,
        ]);
    }
    // view dashboard buku
    public function bookView()
    {
        $books = DB::table('books')->get();
        foreach ($books as $book) {
            $book->genre = DB::table('genres')->where('id', $book->genre_id)->first();
        }
        return Inertia::render('admin/book',[
            'books' => $books,
            'genres' => DB::table('genres')->get(),
        ]);
    }
    // view dashboard otorisasi peminjaman
    public function peminjamanView()
    {
        $borrows = DB::table('borrows')->where('status','requested')->get();
        foreach ($borrows as $borrow) {
            $borrow->user = DB::table('users')->where('id', $borrow->user_id)->first();
            $borrow->book = DB::table('books')->where('id', $borrow->book_id)->first();
        }
        return Inertia::render('admin/peminjaman',[
            'borrows' => $borrows,
        ]);
    }
    // view dashboard otorisasi pengembalian
    public function pengembalianView()
    {
        $borrows = DB::table('borrows')->where('status','returning')->get();
        foreach ($borrows as $borrow) {
            $borrow->user = DB::table('users')->where('id', $borrow->user_id)->first();
            $borrow->book = DB::table('books')->where('id', $borrow->book_id)->first();
        }
        return Inertia::render('admin/pengembalian',[
            'borrows' => $borrows,
        ]);
    }
    // view dashboard history semua peminjaman dan pengembalian
    public function historyView()
    {
        $borrows = DB::table('borrows')->get();
        foreach ($borrows as $borrow) {
            $borrow->user = DB::table('users')->where('id', $borrow->user_id)->first();
            $borrow->book = DB::table('books')->where('id', $borrow->book_id)->first();
        }
        return Inertia::render('admin/history',[
            'borrows' => $borrows,
        ]);
    }
    // view dashboard semua user
    public function userListView()
    {
        return Inertia::render('admin/user',[
            'users' => DB::table('users')->where('is_active',true)->get(),
        ]);
    }
    // view dashboard otorisasi user
    public function userOtorView()
    {
        return Inertia::render('admin/otorisasi',[
            'users' => DB::table('users')->where('is_active',false)->get(),
        ]);
    }
}
