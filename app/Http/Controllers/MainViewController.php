<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Borrow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainViewController extends Controller
{
    // view main index
    public function index()
    {
        return Inertia::render('main/index');
    }
    // view main book
    public function book()
    {
        $books = DB::table('books')->get();
        foreach ($books as $book) {
            $book->genre = DB::table('genres')->where('id', $book->genre_id)->first();
        }
        return Inertia::render('main/book',[
            'books' => $books,
        ]);
    }
    // view main history
    public function history()
    {
        $borrows = DB::table('borrows')->where('user_id',auth()->user()->id)->get();
        foreach ($borrows as $borrow) {
            $borrow->book = DB::table('books')->where('id', $borrow->book_id)->first();
        }
        return Inertia::render('main/history',[
            'borrows' => $borrows,
        ]);
    }
}
