<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\BorrowingBook;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\BorrowBookRequest;
use App\Http\Requests\BorrowBooksRequest;

class BorrowController extends Controller
{
    // user melakukan request peminjaman buku
    public function userBorrowBook(BorrowBookRequest $request)
    {
        $book = DB::table('books')->where('id', $request->id)->first();
        DB::beginTransaction();
        try {
            DB::table('borrows')->insert([
                'user_id' => auth()->user()->id,
                'book_id' => $request->id,
                'amount' => $request->amount > $book->total_pcs - $book->borrowed_pcs ? $book->total_pcs - $book->borrowed_pcs : $request->amount,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            HistoryController::make(auth()->user()->id, "Peminjaman buku : $book->title, berhasil diajukan. Silahkan tunggu konfirmasi admin.", 'info');
            DB::commit();
            return redirect()->back()->with('success', 'Borrow request sent successfully');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }
    // user melakukan request peminjaman banyak buku
    public function userBorrowBooks(BorrowBooksRequest $request)
    {
        DB::beginTransaction();
        try {
            $items = $request->items;
            foreach($items as $item) {
                $book = DB::table('books')->where('id', $item['id'])->first();
                DB::table('borrows')->insert([
                    'user_id' => auth()->user()->id,
                    'book_id' => $item['id'],
                    'amount' => $item['amount'] > $book->total_pcs - $book->borrowed_pcs ? $book->total_pcs - $book->borrowed_pcs : $item['amount'],
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
                HistoryController::make(auth()->user()->id, "Peminjaman buku : $book->title, berhasil diajukan. Silahkan tunggu konfirmasi admin.", 'info');
            }
            DB::commit();
            return redirect()->back()->with('success', 'Borrow request sent successfully');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }
    // user melakukan request pengembalian buku
    public function userReturnBook(Request $request, $borrow)
    {
        DB::beginTransaction();
        try {
            $borrow = DB::table('borrows')->where('id', $borrow)->first();
            $book = DB::table('books')->where('id', $borrow->book_id)->first();
            DB::table('borrows')->where('id', $borrow->id)->update([
                'status' => 'returning',
                'updated_at' => now(),
            ]);
            HistoryController::make(auth()->user()->id, "Pengembalian buku : $book->title, berhasil dilakukan. Silahkan tunggu konfirmasi admin.", 'info');
            DB::commit();
            return redirect()->back()->with('success', 'Book returned successfully');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }
    // admin menyetujui peminjaman buku
    public function adminAcceptBorrowBook(Request $request, $borrow)
    {
        DB::beginTransaction();
        try {
            $borrow = DB::table('borrows')->where('id', $borrow)->first();
            $book = DB::table('books')->where('id', $borrow->book_id)->first();
            DB::table('borrows')->where('id', $borrow->id)->update([
                'status' => $request->status,
                'accepted_at' => $request->status == 'accepted' ? now() : null,
                'rejected_at' => $request->status == 'rejected' ? now() : null,
                'rejected_reason' => $request->status == 'rejected' ? $request->rejected_reason : null,
                'amount' => $book->total_pcs - $book->borrowed_pcs > $borrow->amount ? $borrow->amount : $book->total_pcs - $book->borrowed_pcs,
                'updated_at' => now(),
            ]);
            if($request->status == 'accepted') {
                DB::table('books')->where('id', $borrow->book_id)->update([
                    'borrowed_pcs' => DB::raw('borrowed_pcs + ' . $borrow->amount),
                    'updated_at' => now(),
                ]);
                $id = HistoryController::make($borrow->user_id, "Pengajuan peminjaman buku : $book->title, berhasil disetujui admin. Selamat membaca", 'success');
                if($id != 0) {
                    $data = DB::table('histories')->where('id', $id)->first();
                    event(new BorrowingBook($data->id,$data->user_id, $data));
                }
            } else {
                $id = HistoryController::make($borrow->user_id, "Pengajuan peminjaman buku : $book->title, ditolak admin. Alasan : $request->rejected_reason", 'danger');
                if($id != 0) {
                    $data = DB::table('histories')->where('id', $id)->first();
                    event(new BorrowingBook($data->id,$data->user_id, $data));
                }
            }
            DB::commit();
            return redirect()->back()->with('success', 'Borrow request accepted successfully');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }
    // admin menerima pengembalian buku
    public function adminAcceptReturnBook(Request $request, $borrow)
    {
        DB::beginTransaction();
        try {
            $borrow = DB::table('borrows')->where('id', $borrow)->first();
            $book = DB::table('books')->where('id', $borrow->book_id)->first();
            DB::table('borrows')->where('id', $borrow->id)->update([
                'status' => 'returned',
                'returned_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('books')->where('id', $borrow->book_id)->update([
                'borrowed_pcs' => DB::raw('borrowed_pcs - ' . $borrow->amount),
                'updated_at' => now(),
            ]);
            $id = HistoryController::make($borrow->user_id, "Pengajuan pengembalian buku : $book->title, berhasil diterima admin. Terimakasih telah membaca.", 'success');
            DB::commit();
            if($id != 0) {
                $data = DB::table('histories')->where('id', $id)->first();
                event(new BorrowingBook($data->id,$data->user_id, $data));
            }
            return redirect()->back()->with('success', 'Book returned successfully');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }
}
