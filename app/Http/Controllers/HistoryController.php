<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\BorrowingBook;
use Illuminate\Support\Facades\DB;

class HistoryController extends Controller
{
    // membuat data history
    public static function make($id, $message, $type)
    {
        DB::beginTransaction();
        try {
            $id = DB::table('histories')->insertGetId([
                'user_id' => $id,
                'message' => $message,
                'type' => $type,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::commit();
            return $id;
        } catch (\Throwable $th) {
            DB::rollback();
            return 0;
        }
    }
    // mengambil data history by user id
    public function getData($id)
    {
        $histories = DB::table('histories')->where('user_id', $id)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'histories' => $histories,
        ], 200);
    }
    // mengambil data history by id
    public function getHistory($id)
    {
        DB::beginTransaction();
        try {
            DB::table('histories')->where('id', $id)->update([
                'is_read' => true,
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
        } finally {
            return response()->json([
                'success' => true,
                'history' => DB::table('histories')->where('id', $id)->first(),
            ], 200);
        }
    }
}
