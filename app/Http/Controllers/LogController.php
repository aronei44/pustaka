<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;

class LogController extends Controller
{
    // view login dan register
    public function authView()
    {
        return Inertia::render('auth');
    }
    // user melakukan login
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = auth()->user();
            if($user->is_active == false){
                Auth::logout();
                return redirect()->route('login')->with('server', 'Akun anda belum diaktifkan');
            }
            $request->session()->regenerate();

            return redirect()->intended('/');
        } else {
            return redirect()->back()->with('server', 'Email atau password salah');
        }
    }
    // user melakukan register
    public function register(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            DB::table('users')->insert([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::commit();
            return redirect()->back()->with('success', 'Akun berhasil dibuat, silahkan tunggu admin menyetujui akun anda');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }

        return redirect('/')->with('success', 'Berhasil mendaftar. Silahkan tunggu admin menyetujui akun anda.');
    }
    // user melakukan logout
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->regenerate();

        return redirect('/');
    }
    // admin melakukan aktivasi user
    public function adminAcceptUser($user)
    {
        DB::beginTransaction();
        try {
            DB::table('users')->where('id', $user)->update([
                'is_active' => true,
                'updated_at' => now(),
            ]);
            HistoryController::make($user, 'Akun anda telah diaktifkan. Terimakasih telah bergabung dengan perpustakaan ini.', 'success');
            DB::commit();
            return redirect()->back()->with('success', 'Akun berhasil diaktifkan');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }
}

