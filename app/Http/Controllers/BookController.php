<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // store data buku
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            DB::table('books')->insert([
                'title' => $request->title,
                'author' => $request->author,
                'genre_id' => $request->genre_id,
                'floor' => $request->floor,
                'total_pcs' => $request->total_pcs,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::commit();
            return redirect()->back()->with('success', 'Book Created');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    // update data buku
    public function update(Request $request, $book)
    {
        DB::beginTransaction();
        try {
            DB::table('books')->where('id', $book)->update([
                'title' => $request->title,
                'author' => $request->author,
                'genre_id' => $request->genre_id,
                'floor' => $request->floor,
                'total_pcs' => $request->total_pcs,
                'updated_at' => now(),
            ]);
            DB::commit();
            return redirect()->back()->with('success', 'Book Updated');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    // delete data buku
    public function destroy($book)
    {
        DB::beginTransaction();
        try {
            $this->clear($book);
            DB::table('books')->where('id', $book)->delete();
            DB::commit();
            return redirect()->back()->with('success', 'Book Deleted');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }
    // delete data peminjaman buku
    public static function clear($id)
    {
        DB::beginTransaction();
        try {
            DB::table('borrows')->where('book_id', $id)->delete();
            DB::commit();
            return true;
        } catch (\Throwable $th) {
            DB::rollback();
            return false;
        }
    }
}
