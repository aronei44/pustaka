<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\GenreRequest;

class GenreController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // store data genre
    public function store(GenreRequest $request)
    {
        DB::beginTransaction();
        try {
            DB::table('genres')->insert([
                'name' => $request->name,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::commit();
            return redirect()->back()->with('success', 'Genre Created');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    // update data genre
    public function update(GenreRequest $request, $genre)
    {
        DB::beginTransaction();
        try {
            DB::table('genres')->where('id', $genre)->update([
                'name' => $request->name,
                'updated_at' => now(),
            ]);
            DB::commit();
            return redirect()->back()->with('success', 'Genre Updated');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    // delete data genre
    public function destroy($genre)
    {
        DB::beginTransaction();
        try {
            $genre = DB::table('genres')->where('id', $genre)->first();
            foreach(DB::table('books')->where('genre_id', $genre->id)->get() as $book) {
                BookController::clear($book->id);
                DB::table('books')->where('id', $book->id)->delete();
            }
            DB::table('genres')->where('id', $genre->id)->delete();
            DB::commit();
            return redirect()->back()->with('success', 'Genre Deleted');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('server', $th->getMessage());
        }
    }
}
