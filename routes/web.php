<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainViewController::class, 'index']);
Route::get('/book', [MainViewController::class, 'book']);

Route::group(['middleware' => 'guest', 'prefix'=>'auth'], function () {
    Route::get('/', [LogController::class, 'authView'])->name('login');
    Route::post('/login', [LogController::class, 'login']);
    Route::post('/register', [LogController::class, 'register']);
});

Route::group(['middleware' => 'auth'], function () {
    Route::post('auth/logout', [LogController::class, 'logout']);
    Route::get('/history', [MainViewController::class, 'history']);
    Route::get('/history/data/{id}', [HistoryController::class, 'getData']);
    Route::get('/history/detail/{id}', [HistoryController::class, 'getHistory']);
    Route::group(['middleware' => 'active'], function () {
        Route::post('/book/borrow', [BorrowController::class, 'userBorrowBook']);
        Route::post('/book/borrows', [BorrowController::class, 'userBorrowBooks']);
        Route::post('/book/return/{borrow}', [BorrowController::class, 'userReturnBook']);
    });

    Route::group(['prefix'=>'admin', 'middleware'=>'admin'], function () {
        Route::get('/', [AdminViewController::class, 'index']);

        Route::group(['prefix'=>'genre'], function () {
            Route::get('/', [AdminViewController::class, 'genreView']);
            Route::post('/', [GenreController::class, 'store']);
            Route::put('/{genre}', [GenreController::class, 'update']);
            Route::delete('/{genre}', [GenreController::class, 'destroy']);
        });
        Route::get('/peminjaman', [AdminViewController::class, 'peminjamanView']);
        Route::get('/pengembalian', [AdminViewController::class, 'pengembalianView']);
        Route::get('/history', [AdminViewController::class, 'historyView']);
        Route::group(['prefix'=>'book'], function () {
            Route::get('/', [AdminViewController::class, 'bookView']);
            Route::post('/', [BookController::class, 'store']);
            Route::put('/{book}', [BookController::class, 'update']);
            Route::delete('/{book}', [BookController::class, 'destroy']);
        });

        Route::prefix('user')->group(function () {
            Route::post('/approve/{user}', [LogController::class, 'adminAcceptUser']);
            Route::post('/borrow/{borrow}', [BorrowController::class, 'adminAcceptBorrowBook']);
            Route::post('/return/{borrow}', [BorrowController::class, 'adminAcceptReturnBook']);
        });
        Route::get('/user-list', [AdminViewController::class, 'userListView']);
        Route::get('/otorisasi-user', [AdminViewController::class, 'userOtorView']);
    });
});
