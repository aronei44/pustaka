# Pustaka Digital

By : Aronei44 / Megadigi

## Introduction

Pustaka digital adalah sebuah sistem perpustakaan dimana user bisa request peminjaman untuk kemudian disetujui admin. dan begitu pun saat pengembalian.

## Requirement

- Git Bash
- Vs Code / text editor lainnya
- Xampp PHP ^8
- NodeJs ^v16

## Installation

clone this repository

```
git clone https://gitlab.com/aronei44/pustaka.git
cd pustaka
```

update dependencies

```
composer install or composer update
npm install
```

update environment

```
cp .env.example .env
php artisan key:generate
php artisan migrate --seed
```

run the app

```
php artisan serve

// open other terminal

npm run watch
```

## Credentials

email : admin@gmail.com

password : admin123







